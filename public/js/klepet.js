var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var tmp = ukaz;
  var index = ukaz.indexOf('"');
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  var kanal;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      console.log(besede);
      
      
      if (index == -1) {
        console.log("brez");
        var x = besede.join(' ');
        this.spremeniKanal(x, null);
      break;
      }
      
      console.log(besede.length);
      console.log(besede);
      if (besede.length == 1) {
        console.log("samo kanal");
        kanal = besede.join(' ');
        kanal = kanal.replace(/"/g, "");
        console.log(kanal);
        this.spremeniKanal(kanal, null); 
      } else if (besede.length == 2) {
        //console.log(besede[0] + besede[1]);
        kanal = besede[0].replace(/"/g, "");
        var geslo = besede[1].replace(/"/g, "");
        console.log(kanal + " geslo: " + geslo);
        this.spremeniKanal(kanal, geslo); 
      } else {
        sporocilo = 'Neveljavna sintaksa ukaza.';
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      //besede.join();
      besede = tmp.split('"');
      if (besede.length == 5) {
         //prompt(besede[3] + " | besede 1 " + besede[1]);
        this.socket.emit('posljiZasebnoSporocilo',{sporocilo: besede[3], prejemnik: besede[1]});
      } else {
        sporocilo = 'Neveljaven ukaz.'
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

