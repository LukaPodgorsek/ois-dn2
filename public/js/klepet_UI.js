function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function divElementTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);  
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  
  if (sporocilo.charAt(0) == '<') {
    return;
  }
  
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = dodajSmajlije(sporocilo);
    var besede = sporocilo.split(' ');
    sporocilo = preveriSporocilo(besede);
    sporocilo = sporocilo.join();
    console.log(sporocilo);
    sporocilo = sporocilo.replace(/,/g, " ");
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function dodajSmajlije(sporocilo) {
  //sporocilo = sporocilo.replace("<","&lt");
  //sporocilo = sporocilo.replace(">","&gt");
  sporocilo = sporocilo.replace(/\;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>');
  sporocilo = sporocilo.replace(/\:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>');
  sporocilo = sporocilo.replace(/\:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>');
  sporocilo = sporocilo.replace(/\:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>');
  sporocilo = sporocilo.replace(/\(\y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>');
  return sporocilo;
}

function loadXMLDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET",'swearWords.xml',false);
  xhttp.send();
  return xhttp.responseXML;
}

function parseXml() {
    var xml = loadXMLDoc();
    var tmp = xml.getElementsByTagName("word");
    for (var j = 0; j < tmp.length; j++) {
      var x = tmp[j].childNodes[0].nodeValue;
      vulgarneBesede[x] = x.length;
    }
    //console.log(xml);
    //console.log(vulgarneBesede);
}

function generirajZvezdice(value) {
  return new Array(value + 1).join("*");
}

function preveriSporocilo(besede) {
  for (var i in besede) {
    var tmp = besede[i].toLowerCase();
    if (tmp in vulgarneBesede) {
      var replace = generirajZvezdice(vulgarneBesede[tmp]);
      besede[i] = replace;
    }
  }
  return besede;
}

var vulgarneBesede = {};
var socket = io.connect();
var trenutniKanal;
var kanal;

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    parseXml();
    kanal = rezultat.kanal;
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  socket.on('obdelajKanal', function(objekt){
    var text = objekt.vzdevek + " @ " + objekt.kanal;
    trenutniKanal = objekt.kanal;
    $('#kanal').text(text);
  });

  socket.on('sporocilo', function (sporocilo) {
    var tekst = sporocilo.besedilo;
    tekst = tekst.split(' ');
    tekst = preveriSporocilo(tekst);
    tekst = tekst.join();
    tekst = tekst.replace(/,/g, " ");
    tekst = dodajSmajlije(tekst);
    console.log(tekst);
    var tmp = divElementTekst(tekst);
    $('#sporocila').append(tmp);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  socket.on('uporabniki', function(uporabniki){
    $('#seznam-uporabnikov').empty();
    for (var i in uporabniki) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
  });
  
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabnikiServer', kanal);
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});